import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';   // package imported from react (reusable modules)
import 'bootstrap/dist/css/bootstrap.min.css';

import App from './App';

ReactDOM.render(
  // parameters: (what to render, where to render)
  // <Fragment></Fragment> used as <div> for React.js: needs to be imported from React.js module
  // parameters: (<App />, document.getElementById('root'))
  <Fragment>
    <App /> 
  </Fragment>,document.getElementById('root')
  
);