let coursesData = [
    {
        id: `WDC001`,
        name: `PHP-Laravel`,
        description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat.`,
        price: 25000,
        onOffer: true
    },
    {
        id: `WDC002`,
        name: `Python-Django`,
        description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat.`,
        price: 35000,
        onOffer: true
    },
    {
        id: `WDC003`,
        name: `Java-Sprinboot`,
        description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat.`,
        price: 45000,
        onOffer: true
    },
    {
        id: `WDC004`,
        name: `NodeJs-ExpressJs`,
        description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat.`,
        price: 55000,
        onOffer: true
    }
]

export default coursesData