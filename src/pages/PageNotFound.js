import Banner from "./../components/Banner";


export default function PageNotFound(){
    const data = {
        title: "Error 404",
        description: "Page not found",
        destination: "/home",
        buttonDesc: "Go Back Home"
    }
    return <Banner bannerProp={data}/>
    
}