import CourseCard from "./../components/CourseCard";
import coursesData from "../mockData/courses";
import { Fragment, useContext } from "react";
import UserContext from "../UserContext";

export default function Course(){
    // console.log(coursesData[0])
    const {user} = useContext(UserContext)
    console.log(user)


    const courses = coursesData.map(course => {
        return <CourseCard key={course.id} courseProp={course}/>
    })

    return (
        <Fragment>
            {courses}
        </Fragment>
    )
}