import {Form, Button, Row, Col, Container} from "react-bootstrap"
import {useEffect, useState, useContext} from "react"
import {useNavigate} from "react-router-dom"

// App State Management
import UserContext from "../UserContext";


export default function Register(){
    const [fN, setFN] = useState("")
    const [lN, setLN] = useState("")
    const [email, setEmail] = useState("")
    const [pw, setPw] = useState("")
    const [cpw, setCpw] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)

    const navigate = useNavigate();

    const {user} = useContext(UserContext)
    useEffect(()=> {
        if(user.id !== null){
            navigate(`/courses`)
        }
    }, [user.id])
    
    // useEffect(function, options)
    useEffect(() => {
        // console.log(`render`)
        // if all  input fields are empty, keep the state of the button to  true
        // else if all fields are filled out, change the state to false
        if((fN !== "" && lN !== "" && email !== "" && pw !== "" && cpw !== "") && (pw == cpw)){
            setIsDisabled(false)
        } else {
            setIsDisabled(true)
        }

        // listen to state changes: fn, ln, email, pw, cpw
    }, [fN, lN, email, pw, cpw])

    const registerUser = (e) => {
        e.preventDefault()
        // console.log(e)
        fetch(`http://localhost:3007/api/users/email-exists`,{
            method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
			})
        })
        .then(response => response.json())
        .then(response => {
            // console.log(response)
            if(!response){
                fetch(`http://localhost:3007/api/users/register`,{
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    firstName: fN,
                    lastName: lN,
                    email: email,
                    password: pw
                })
                })
                .then(response => response.json())
                .then(response => {
                    // console.log(response)
                    if(response){
                        alert(`Registration Successful!`)

                        // riderect
                        navigate(`/login`)
                    } else {
                        alert(`Something went wrong. Try again.`)
                    }
                })

            } else{
                alert(`User already exists.`)
            }
        })
    }
    return(
       <Container className="m-5">
           <Row className="justify-content-center">
           <Col xs={10} md={6}>
                <Form onSubmit={(e) => registerUser(e)}>
                    <h3 className="text-center p-3">REGISTER</h3>
                    <Form.Group className="mb-3">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control 
                            type="text" 
                            value={fN} 
                            onChange = {(e) => setFN(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control 
                            type="text" 
                            value={lN} 
                            onChange = {(e) => setLN(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            value={email} 
                            onChange = {(e) => setEmail(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            value={pw}
                            onChange = {(e) => setPw(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Verify Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            value={cpw} 
                            onChange = {(e) => setCpw(e.target.value)}
                        />
                    </Form.Group>
                    <Button 
                        variant="info" 
                        type="submit"
                        disabled = {isDisabled}
                    >
                        Submit
                    </Button>
                </Form>
           </Col>
       </Row>
       </Container>
    )
}