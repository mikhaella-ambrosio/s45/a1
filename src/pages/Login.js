import {useState, useEffect} from 'react'
import {Form, Button, Row, Col, Container} from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'

export default function Login(){
	const [email, setEmail] = useState("")
	const [pw, setPW] = useState("")
	const [isDisabled, setIsDisabled] = useState(true)

	const navigate = useNavigate()

	useEffect(() => {
		if(email !== "" && pw !== ""){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [email, pw])

	const loginUser = async (e) => {
		e.preventDefault()

		await fetch('http://localhost:3007/api/users/login', {
			method: "POST",
			headers:{
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: pw
			})
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				localStorage.setItem('token', response.token)
				localStorage.setItem('email', email)

				// console.log(localStorage.getItem(email))
				setEmail("")
				setPW("")


				navigate('/courses')

			} else {
				alert('Incorrect credentials!')
			}
		})
	}


	return(
		<Container className="m-5">
		 	<h3 className="text-center">Login</h3>
			<Row className="justify-content-center">
				<Col xs={12} md={6}>
					<Form onSubmit={(e) => loginUser(e) }>
						<Form.Group className="mb-3">
							<Form.Label>Email address</Form.Label>
					    	<Form.Control 
					    		type="email" 
					    		value={email}
					    		onChange={(e) => setEmail(e.target.value)}
					    	/>
						</Form.Group>

						<Form.Group className="mb-3">
					    	<Form.Label>Password</Form.Label>
					    	<Form.Control 
					    		type="password" 
					    		value={pw}
					    		onChange={(e) => setPW(e.target.value)}
					    	/>
						</Form.Group>

						<Button 
							variant="info" 
							type="submit"
							disabled={isDisabled}
						>
							Submit
						</Button>
					</Form>
				</Col>
			</Row>
		</Container>
	)
}


//////////////////////////////////////////////////////////////

// import {Container, Row, Col, Form, Button} from "react-bootstrap"
// import {useEffect, useState} from "react"
// import {useNavigate} from "react-router-dom"


// export default function Login(){

//     const [email, setEmail] = useState("")
//     const [pw, setPw] = useState("")
//     const [isDisabled, setIsDisabled] = useState(true)

//     const navigate = useNavigate();


//     useEffect(() => {
//         // console.log(`render`)
//         if(email !== "" && pw !== "" ){
//             setIsDisabled(false)
//         } else {
//             setIsDisabled(true)
//         }
//     }, [email, pw])
    
//     const loginUser = async (e) => {
//         e.preventDefault()

//         await fetch('http://localhost:3007/api/users/login', {
//             method: "POST",
//             headers: {
//                 "Content-Type": "application/json"
//             },
//             body: JSON.stringify({
//                 email: email,
//                 password: pw
//             })
//             .then(response => response.json())
//             .then(response => {
//                 console.log(response)


//                 if(response){
//                     localStorage.setItem(`token`, response.token)
//                     localStorage.setItem(`email`, email)
//                     // alert(`Login Succesfully!`)

//                     setEmail("")
//                     setPw("")

//                     navigate(`/courses`)
//                 } else {
//                     alert(`Incorrect Credentials!`)
//                 }
//             })
//         })
//     }

//     return(
//         <Container className="m-5 ">
//            <Row className="justify-content-center">
//            <Col xs={10} md={6}>
//                 <Form onSubmit={(e) => loginUser(e)}>
//                     <h3 className="text-center p-3">LOGIN</h3>
//                     <Form.Group className="mb-3">
//                         <Form.Label>Email Address</Form.Label>
//                         <Form.Control 
//                             type="email" 
//                             value={email} 
//                             onChange = {(e) => setEmail(e.target.value)}
//                         />
//                     </Form.Group>
//                     <Form.Group className="mb-3">
//                         <Form.Label>Password</Form.Label>
//                         <Form.Control 
//                             type="password" 
//                             value={pw}
//                             onChange = {(e) => setPw(e.target.value)}
//                         />
//                     </Form.Group>
//                     <Button 
//                         variant="info" 
//                         type="submit"
//                         disabled = {isDisabled}
//                     >
//                         Login
//                     </Button>
//                 </Form>
//            </Col>
//        </Row>
//        </Container>
//     )
// }