import { useState } from "react";
import {
  Routes,
  Route,
  BrowserRouter,
} from "react-router-dom";

import { UserProvider } from "./UserContext"

import Home from "./pages/Home"
import Courses from "./pages/Courses"
import Register from "./pages/Register"
import Login from "./pages/Login"
import Footer from "./components/Footer";
import AppNavbar from "./components/AppNavbar";
import PageNotFound from "./pages/PageNotFound";


// JSX: JavaScript 
function App() {

  const [user, setUser] = useState({
    id: 123,
    isAdmin: true
  })
  
  return(
    <UserProvider value={{user, setUser}}>
      <BrowserRouter>
      <AppNavbar/>
      <Routes>
        <Route exact path="/home" element={ <Home/> }/>
        <Route exact path="/courses" element={ <Courses/> }/>
        <Route exact path="/register" element={ <Register/> }/>
        <Route exact path="/login" element={ <Login/> }/>
        <Route path="*" element={ <PageNotFound/> }/>
      </Routes>
      <Footer/>
      </BrowserRouter>
    </UserProvider>    
  )
}

export default App;
