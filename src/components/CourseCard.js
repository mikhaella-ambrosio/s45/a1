import {Card, Button} from "react-bootstrap";
import {useState, useEffect} from "react"

export default function CourseCard({courseProp}){
    // console.log(courseProp)

    let [count, setCount] = useState(0)
    let [seats, setSeat] = useState(30)
    let [enrollees, setEnrollees] = useState(0)
        // count = variable ; setCount = function that changes the variable
    const {name, description, price} = courseProp
    // console.log(name)
    // console.log(description)
    // console.log(price)
    // useEffect(() => {console.log(`render`)}, [])
    const handleClick = () => {
        // console.log(`Im clicked!`, count+1)
        if(seats == 0){
            alert(`There are no more seats available for this course.`)
        } else{
            if( seats>0 && seats<=30){
                setCount(count+1)
                setEnrollees(enrollees+1)
                setSeat(seats-1)
            }
        }
    }
    return (
        <Card className="m-5">
        <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>
                <p>{description}</p>
            </Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>
                <p>{price}</p>
            </Card.Text>
            <Card.Text>Count: {count}</Card.Text>
            <Card.Subtitle>Seats Available:</Card.Subtitle>
            <Card.Text>
                <p>{seats} Seats</p>
            </Card.Text>
            <Card.Subtitle>Enrollees:</Card.Subtitle>
            <Card.Text>
                <p>{enrollees} Enrollees</p>
            </Card.Text>
            <Button variant="info" onClick={handleClick}>Enroll</Button>
        </Card.Body>
        </Card>
    )
}