export default function Footer(){
    return(
        // FOOTEER (bootstrap)
    <div className="bg-info fixed-bottom text-white d-flex justify-content-center align-items-center" style={{height: `10vh`, marginTop: `50px`}}>
        <p className="m-0 font-weight-bold">Mikhaella &#64; Course Bookling | Zuitt Coding Bootcamp &#169;</p>
    </div>
    )
}